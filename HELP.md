# Getting Started

Create Table:
-------------
create table mortgage_rate
(
mortgage_id integer not null,
maturity_period integer not null,
interest_rate varchar(10),
last_update timestamp,
primary key (mortgage_id)
);

Insert table:
-------------
insert into mortgage_rate (mortgage_id, maturity_period, interest_rate, last_update) values(1001, 15, '9.10%', '2023-05-17 15:36:39');



Get interest rates:
-------------------
Method : GET
http://localhost:9090/api/interest-rates

Check Mortgage:
--------------
Method : POST
http://localhost:9090/api/mortgage-check

Json Body:

{
	"income":10000,
    "maturityPeriod":10,
    "loanValue":100000,
    "homeValue":1000000,
	"interestRate":10.50
}