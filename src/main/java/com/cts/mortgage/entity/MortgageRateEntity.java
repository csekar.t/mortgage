/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cts.mortgage.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author ChandraSekar
 */
@Entity
@Table(name = "mortgage_rate")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MortgageRateEntity implements Serializable
{
   @Id
   @Column(name = "mortgage_id")
   private int mortgageId;
   
   @Column(name = "maturity_period")
   private int maturityPeriod;
   
   @Column(name = "interest_rate")
   private String interestRate;
   
   @Column(name = "last_update")
   private String lastUpdate;
}
