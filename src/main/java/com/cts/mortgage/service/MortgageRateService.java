/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cts.mortgage.service;

import com.cts.mortgage.dto.MortgageRates;
import com.cts.mortgage.entity.MortgageRateEntity;
import com.cts.mortgage.repository.MortgageRateRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ChandraSekar
 */
@NoArgsConstructor
@AllArgsConstructor
@Service
public class MortgageRateService
{
    @Autowired
    private MortgageRateRepository mortgageRateRepository;
    @Autowired
    private MortgageRates mortGageRateDto;
    
    public MortgageRates getMortgageRates()
    {
        try
        {
        List<MortgageRateEntity> mortgageRates = mortgageRateRepository.findAll();
        System.out.println(mortgageRates);
        mortGageRateDto.setMortGageRateList(mortgageRates);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return mortGageRateDto;
    }
}
