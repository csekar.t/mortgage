/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cts.mortgage.service;

import com.cts.mortgage.dto.CheckMortgageDto;
import com.cts.mortgage.info.LoanInfo;
import java.text.DecimalFormat;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ChandraSekar
 */
@Service
@NoArgsConstructor
public class CheckMortgageService
{

    @Autowired
    private CheckMortgageDto checkMortgageDto;
    public CheckMortgageDto getMortGage(LoanInfo loanInfo)
    {
        boolean isMortgage = isMortgage(loanInfo);
        double monthlyEmi = 0d;
        if(isMortgage == true)
        {
            monthlyEmi = calculateMonthlyPayment(loanInfo);
        }
        checkMortgageDto.setIsMortgage(isMortgage);
        checkMortgageDto.setMonthlyEmi(monthlyEmi);
        return checkMortgageDto;
    }
    
    public boolean isMortgage(LoanInfo loanInfo)
    {
        boolean mortgageFlag = false;
        double loanValue = loanInfo.getLoanValue();
        if (loanValue > 0)
        {
            //mortgage should not exceed 4 times the income
            double income = loanInfo.getIncome();
            if (income > 0)
            {
                double multiply4 = 4d * income;
                if (loanValue < multiply4)
                {
                    mortgageFlag = true;
                }
            }

            //mortgage should not exceed the home value
            double homeValue = loanInfo.getHomeValue();
            if (mortgageFlag == true && homeValue > 0)
            {
                if (loanValue < homeValue)
                {
                    mortgageFlag = true;
                }
            }

        }
        return mortgageFlag;
    }

    public double calculateMonthlyPayment(LoanInfo loanInfo)
    {
        double interestRate = loanInfo.getInterestRate() / (12 * 100);
        double principleAmount = loanInfo.getLoanValue();
        double tenurMonths = loanInfo.getMaturityPeriod() * 12;
        double emi = (principleAmount * interestRate * Math.pow(1 + interestRate, tenurMonths)) / (Math.pow(1 + interestRate, tenurMonths) - 1);
        return Math.round(emi * 100.0) / 100.0;
    }
}
