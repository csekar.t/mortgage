/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cts.mortgage.info;

import lombok.Data;

/**
 *
 * @author INDIA
 */
@Data
public class LoanInfo
{
    private double income;
    private int maturityPeriod;
    private double loanValue;
    private double homeValue;
    private double interestRate;
}
