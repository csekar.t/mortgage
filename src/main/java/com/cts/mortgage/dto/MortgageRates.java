/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cts.mortgage.dto;

import com.cts.mortgage.entity.MortgageRateEntity;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 *
 * @author ChandraSekar
 */
@NoArgsConstructor
@Data
@Component
public class MortgageRates implements Serializable
{
     private List<MortgageRateEntity> mortGageRateList;
}
