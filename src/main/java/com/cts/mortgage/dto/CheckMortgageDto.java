/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cts.mortgage.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 *
 * @author ChandraSekar
 */
@Data
@NoArgsConstructor
@Component
public class CheckMortgageDto
{
    private Boolean isMortgage;
    private double monthlyEmi;
}
