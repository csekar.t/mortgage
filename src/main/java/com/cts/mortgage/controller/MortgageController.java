/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cts.mortgage.controller;

import com.cts.mortgage.dto.CheckMortgageDto;
import com.cts.mortgage.dto.MortgageRates;
import com.cts.mortgage.info.LoanInfo;
import com.cts.mortgage.service.CheckMortgageService;
import com.cts.mortgage.service.MortgageRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ChandraSekar
 */
@RestController
@RequestMapping("/api")
public class MortgageController
{
    @Autowired
    private MortgageRateService mortgageRateService;
    
    @Autowired
    private CheckMortgageService checkMortgageService;
    @GetMapping("/interest-rates")
    public MortgageRates getInterstRates()
    {
        return mortgageRateService.getMortgageRates();
    }
    
    @PostMapping("/mortgage-check")
    public CheckMortgageDto mortgageCheck(@RequestBody LoanInfo loanInfo)
    {
        return checkMortgageService.getMortGage(loanInfo); 
    }
}
